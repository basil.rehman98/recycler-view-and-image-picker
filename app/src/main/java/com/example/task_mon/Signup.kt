package com.example.task_mon

import android.content.Intent
import android.graphics.Bitmap
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.view.View
import android.widget.Button
import android.widget.ImageView
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_signup.*


class Signup : AppCompatActivity() {
    @RequiresApi(Build.VERSION_CODES.N)
    val SELECT_IMAGE = 1
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signup)
        val Register = findViewById<View>(R.id.register) as Button
        Register.setOnClickListener{
            val intent = Intent(this, home::class.java)
            startActivity(intent)
        }
        val pic = findViewById<ImageView>(R.id.changepic)
        pic.setOnClickListener {
//        val intent2 = Intent(
//            Intent.ACTION_PICK,
//            android.provider.MediaStore.Images.Media.INTERNAL_CONTENT_URI
//        )
//            startActivity(intent2)
        //val ACTIVITY_SELECT_IMAGE = 1234
//            val intent = Intent()
//            intent.type = "image/*"
//            intent.action = Intent.ACTION_GET_CONTENT
//            startActivityForResult(Intent.createChooser(intent, "Select Picture"), SELECT_IMAGE)
        var i = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            startActivityForResult(i,123)
        }
    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode == 123){
            var bmp = data?.extras?.get("data") as Bitmap
            changepic.setImageBitmap(bmp)
        }
    }
}