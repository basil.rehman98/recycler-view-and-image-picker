package com.example.task_mon

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Adapter
import android.widget.ArrayAdapter
import android.widget.LinearLayout
import android.widget.ListView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.Adapter as Adapter1

class home : AppCompatActivity() {

    @SuppressLint("WrongConstant")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        val recycle = findViewById<View>(R.id.view_list) as RecyclerView
        recycle.layoutManager = LinearLayoutManager(this, LinearLayout.VERTICAL, false)
        val users = ArrayList<User>()
        users.add(User("Rehan","Karimabad"))
        users.add(User("Rehan","Karimabad"))
        users.add(User("Rehan","Karimabad"))
        users.add(User("Rehan","Karimabad"))
        users.add(User("Rehan","Karimabad"))
        users.add(User("Rehan","Karimabad"))
        val adapter = CustomAdapter(users)

        recycle.adapter = adapter
    }
}