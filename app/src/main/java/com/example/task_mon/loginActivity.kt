package com.example.task_mon

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button

class loginActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        val button = findViewById<View>(R.id.signin) as Button
        button.setOnClickListener{
            val intent = Intent(this, home :: class.java)
            startActivity(intent)
        }
        val textview = findViewById<View>(R.id.signup)
        textview.setOnClickListener {
            val intent2 = Intent ( this, Signup :: class.java )
            startActivity(intent2)
        }
    }
}