package com.example.task_mon

import android.view.LayoutInflater
import android.view.TextureView
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class CustomAdapter(val userlist : ArrayList<User>) : RecyclerView.Adapter<CustomAdapter.ViewHolder>() {
    class ViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView ){
        val textviewname = itemView.findViewById<View>(R.id.list_title) as TextView
        val descip = itemView.findViewById<View>(R.id.list_description) as TextView
    }

    override fun onCreateViewHolder(p0: ViewGroup, viewType: Int): ViewHolder {
       val v = LayoutInflater.from(p0.context).inflate(R.layout.list_item, p0, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: ViewHolder, p1: Int) {
        val user : User = userlist[p1]
        holder?.textviewname?.text = user.name
        holder?.descip?.text = user.address
    }

    override fun getItemCount(): Int {
        return userlist.size
    }
}